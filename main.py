# -*- coding: utf-8 -*-
import sys
import argparse
import psycopg2
import time

def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('name')
    return parser

def Checker(number):

    try:
        conn = psycopg2.connect(dbname='bd_nums', user='postgres', host='localhost', password='admin')
    except:
        print("Ошибка подключения к базе данных")
    cursor = conn.cursor()
    cursor.execute('SELECT nums.numbers FROM nums WHERE numbers=%s', (number,))
    results = cursor.fetchall()
    if (results == []):
        return 0
    else:
        return 1
    conn.close()

if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args(sys.argv[1:])
    start_time = time.time()
    print(Checker(namespace.name))
    print("--- %s seconds ---" % (time.time() - start_time))